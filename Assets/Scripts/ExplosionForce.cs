using UnityEngine;

public class ExplosionForce : MonoBehaviour
{
    public float radius = 4.0F;
    public float power = 10.0F;
    public float lift = 0;
    bool spawnText = true;
    void Update()
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position, radius);

        foreach (Collider hit in colliders)
        {
            if (hit.transform.tag == "Player")
            {
                float dis = (hit.transform.position - transform.position).magnitude;
                float value = 1f - dis / radius;
                if (value >= 0.2f)
                {
                    if (spawnText)
                    {
                        GameManager.instance.handlePoint(false, 10, hit.transform.position);
                        spawnText = false;
                    }
                    Ragdolls.Instance.GetExplosion(transform.position, power, radius, lift);
                }
            }
        }

        Destroy(gameObject, 0.3f);
    }
}
