using UnityEngine;

public class RopeSpawner : MonoBehaviour
{
    [SerializeField]
    GameObject prefab, parent;

    [SerializeField]
    Rigidbody[] boxes;

    [SerializeField]
    float partDistance = 1f;

    [SerializeField]
    [Range(1, 50)] int length = 1;

    [SerializeField]
    bool reset, spawn, snapFirst, snapLast;

    private void Start()
    {
        Spawn();
    }

    private void Update()
    {
        if (reset)
        {
            foreach (GameObject obj in transform.GetComponentsInChildren<GameObject>())
            {
                Destroy(obj);
            }
            reset = false;
        }
    }

    private void Spawn()
    {
        int count = (int)(length / partDistance);

        for (int i = 0; i < count; i++)
        {
            GameObject obj;
            obj = GameObject.Instantiate(prefab, new Vector3(parent.transform.position.x + partDistance * (i + 1), parent.transform.position.y, parent.transform.position.z), Quaternion.identity, parent.transform);
            obj.transform.eulerAngles = new Vector3(0, 0, 90);
            obj.name = parent.transform.childCount.ToString();

            if (i == 0)
            {
                obj.GetComponent<CharacterJoint>().connectedBody = boxes[0];
            }
            else
            {
                obj.GetComponent<CharacterJoint>().connectedBody = parent.transform.Find((parent.transform.childCount - 1).ToString()).GetComponent<Rigidbody>();
            }

            if (i == count - 1)
            {
                boxes[1].GetComponent<CharacterJoint>().connectedBody = obj.GetComponent<Rigidbody>();
            }
        }
    }
}
