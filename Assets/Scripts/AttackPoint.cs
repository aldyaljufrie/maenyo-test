using UnityEngine;

public class AttackPoint : MonoBehaviour
{
    public float force = 20f;

    private void OnTriggerEnter(Collider other)
    {
        if (Ragdolls.Instance.onAttack)
        {
            if (other.gameObject.tag == "Bomb")
            {
                Rigidbody rb = other.GetComponent<Rigidbody>();
                Vector3 dir = other.transform.position - transform.position;
                rb.AddForce(dir.normalized * force, ForceMode.Impulse);
            }
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (Ragdolls.Instance.state == Ragdolls.PlayerState.ALIVE && Ragdolls.Instance.onAttack)
        {
            if (other.gameObject.tag == "Bomb")
            {
                Rigidbody rb = other.GetComponent<Rigidbody>();
                Vector3 dir = other.transform.position - transform.position;
                rb.AddForce(dir.normalized * force, ForceMode.Impulse);
            }
        }
    }
}
