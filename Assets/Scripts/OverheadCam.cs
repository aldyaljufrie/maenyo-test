using UnityEngine;

public class OverheadCam : MonoBehaviour
{
    public Transform target;
    public float height = 15f;
    public float distance = 20f;
    public float angle = 0f;
    public float rotationSpeed = 4f;
    public float smoothFactor = 0.4f;

    private Vector3 velocity;
    private Vector3 prevPos;

    private void Start()
    {
        FollowPlayer();
    }

    private void LateUpdate()
    {
        FollowPlayer();
    }

    private void FollowPlayer()
    {
        if (!target) return;

        Vector3 worldPos = (Vector3.forward * -distance) + (Vector3.up * height);

        // if (Input.GetMouseButton(0))
        //     angle += (Input.GetAxis("Mouse X") * rotationSpeed);

        Vector3 wordRot = Quaternion.AngleAxis(angle, Vector3.up) * worldPos;


        Vector3 targetPos = target.position;

        Vector3 finalPos = targetPos + wordRot;

        transform.position = Vector3.SmoothDamp(transform.position, finalPos, ref velocity, smoothFactor);
        transform.LookAt(targetPos);
    }
}
