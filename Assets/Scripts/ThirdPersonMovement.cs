using UnityEngine;
using Lean.Gui;

public class ThirdPersonMovement : MonoBehaviour
{
    public LeanJoystick joystick;
    public bool MainPlayer = false;
    public CharacterController controller;
    public float speed = 8f;
    public float jumpForce = 2.5f;
    public float smoothFactor = 0.1f;
    public bool isGrounded;
    public Transform groundCheck;
    public float groundDistance = 0.1f;
    public LayerMask groundMask;
    private float angularVelocity;
    private Transform cam;
    private Animator anim;
    private Vector3 moveDir;
    private bool isJumping = false;

    private void Awake()
    {
        if (!MainPlayer) enabled = false;
        cam = Camera.main.transform;
        anim = GetComponent<Animator>();
    }

    private void Update()
    {
        if (GameManager.instance.onPause) return;
        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);
        anim.SetBool("isGrounded", isGrounded);
        HandleMovements();
    }

    private void FixedUpdate()
    {
        if (GameManager.instance.onPause) return;
        moveDir.y = (isGrounded && moveDir.y < 0f) ? Physics.gravity.y * Time.deltaTime : moveDir.y + Physics.gravity.y * Time.deltaTime;
    }

    private Vector3 GetInput()
    {
        if (Ragdolls.Instance.state != Ragdolls.PlayerState.ALIVE) return Vector3.zero;
        float hInput = joystick.ScaledValue.x;
        float vInput = joystick.ScaledValue.y;
        return new Vector3(hInput, 0f, vInput).normalized;
    }

    private void HandleMovements()
    {
        Vector3 dir = GetInput();

        if (isGrounded)
        {
            if (dir.magnitude >= 0.1f)
            {
                float rot = Mathf.Atan2(dir.x, dir.z) * Mathf.Rad2Deg + cam.eulerAngles.y;
                float targetRot = Mathf.SmoothDampAngle(transform.eulerAngles.y, rot, ref angularVelocity, smoothFactor);
                transform.rotation = Quaternion.Euler(0f, targetRot, 0f);
                moveDir = Quaternion.Euler(0f, rot, 0f) * Vector3.forward;
            }
            else moveDir = new Vector3(0f, moveDir.y, 0f);

            if (isJumping) moveDir.y = jumpForce;

            isJumping = false;
        }

        controller.Move(moveDir * speed * Time.deltaTime);
        anim.SetFloat("Velocity", new Vector3(moveDir.x, 0f, moveDir.z).magnitude, 0.1f, Time.deltaTime);
        anim.SetFloat("VelocityY", moveDir.y, 0.1f, Time.deltaTime);
    }

    public void Attack()
    {
        anim.SetTrigger("Attack 1");
    }

    public void Jump()
    {
        if (isGrounded) isJumping = true;
    }
}
