using UnityEngine;
using System.Collections.Generic;

public class BombSpawner : MonoBehaviour
{
    public static BombSpawner Instance;
    public int spawnLimit = 1;
    public float spawnDelay = 2f;
    public float diffDelay = 16f;
    bool spawn = true;
    public float spawnTimer, diffTimer;
    private float spawnRadius = 15f;
    int spawnCount;
    int maxSpawnLimit = 16;

    private void Awake()
    {
        Instance = this;
    }

    void FixedUpdate()
    {
        if (!GameManager.instance.onPause)
        {
            diffTimer += Time.deltaTime;

            if (diffTimer >= diffDelay)
            {
                diffTimer = 0f;
                if (spawnLimit < maxSpawnLimit) spawnLimit++;
            }

            if (spawn)
            {
                spawnTimer += Time.deltaTime;

                if (spawnTimer > spawnDelay)
                {
                    if (spawnCount < spawnLimit)
                    {
                        Spawn();
                        spawnTimer = 0f;
                    }
                    else
                    {
                        spawn = false;
                    }
                }
            }
        }
    }

    public void Pause()
    {
        spawnTimer = 0f;
        spawnLimit = 1;
        diffTimer = 0f;
        spawn = false;
        spawnCount = 0;
    }

    public void Play()
    {
        spawn = true;

        List<GameObject> pooled = GetComponent<ObjectPool>().pooledObjects;
        foreach (var obj in pooled)
        {
            obj.SetActive(false);
        }
    }

    void Spawn()
    {
        GameObject bomb = GetComponent<ObjectPool>().GetPooledObject();
        if (bomb)
        {
            bomb.transform.position = GetSpawnPosition();
            bomb.GetComponent<Dabomb>().spawner = this;
            bomb.SetActive(true);
            spawnCount++;
        }
    }

    Vector3 GetSpawnPosition()
    {
        Vector3 pos = Random.insideUnitCircle * spawnRadius;
        return new Vector3(pos.x, 20f, pos.y);
    }

    public void Despawn()
    {
        spawnCount--;
        spawn = true;
    }
}
