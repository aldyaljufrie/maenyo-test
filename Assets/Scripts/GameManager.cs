using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    public Camera cam;
    public Button button;
    public Text pointText, timerText;
    public TextMesh floatingText;
    public float Timer = 0f;
    public int Point = 0;
    public bool onPause = true;
    string minutes;
    string seconds;

    private void Awake()
    {
        instance = this;
        cam = Camera.main;
    }

    private void Update()
    {
        if (!onPause)
        {
            Timer += Time.deltaTime;
            float m = Mathf.Floor(Timer / 60);
            float s = Mathf.RoundToInt(Timer % 60);
            minutes = m.ToString();
            seconds = s.ToString();

            if (m < 10) minutes = "0" + m.ToString();
            if (s < 10) seconds = "0" + Mathf.RoundToInt(s).ToString();

            timerText.text = minutes + ":" + seconds;
            pointText.text = "Points: " + Point.ToString();

            if (Ragdolls.Instance.state == Ragdolls.PlayerState.DEAD) PauseGame();
            if (Point < 0) Ragdolls.Instance.state = Ragdolls.PlayerState.DEAD;
        }
    }

    public void handlePoint(bool isAdd, int value, Vector3 pos)
    {
        Vector3 newPos = new Vector3(pos.x, pos.y + 3f, pos.z);
        var ft = Instantiate(floatingText, newPos, Quaternion.LookRotation(newPos - cam.transform.position), Ragdolls.Instance.transform);
        ft.text = isAdd ? "+" + value.ToString() : "-" + value.ToString();
        ft.color = isAdd ? Color.green : Color.red;
        Point = isAdd ? Point + value : Point - value;
    }
    void OnGUI()
    {
        GUI.Label(new Rect(100, 50, 100, 100), Mathf.RoundToInt(1.0f / Time.smoothDeltaTime).ToString());
    }

    public void StartGame()
    {
        onPause = false;
        Timer = 0f;
        Point = 0;
        Ragdolls.Instance.transform.position = Ragdolls.Instance.SpawnPoint.position;
        Ragdolls.Instance.state = Ragdolls.PlayerState.ALIVE;
        cam.GetComponent<OverheadCam>().target = Ragdolls.Instance.transform;
        button.gameObject.SetActive(false);
        Spawner.Instance.Play();
        BombSpawner.Instance.Play();
    }

    public void PauseGame()
    {
        onPause = true;
        button.gameObject.SetActive(true);
        Spawner.Instance.Pause();
        BombSpawner.Instance.Pause();
    }
}
