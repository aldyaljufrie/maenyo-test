using UnityEngine;

public class Explosion : MonoBehaviour
{
    public bool explode = false;
    public ParticleSystem explosionParticle;
    Dabomb bomb;

    void Awake()
    {
        bomb = transform.root.GetComponent<Dabomb>();
    }

    void Update()
    {
        if (explode)
        {
            explode = false;
            bomb.Despawn();
            Instantiate(explosionParticle, bomb.transform.position, Quaternion.identity);
        }
    }
}