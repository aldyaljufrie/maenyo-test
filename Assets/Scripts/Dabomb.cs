using UnityEngine;


public class Dabomb : MonoBehaviour
{
    public Explosion explosion;
    public BombSpawner spawner;
    public float time;
    public bool tick = false;
    private Animator anim;
    private float explodeTime = 4f;
    Rigidbody rb;

    private void Start()
    {
        explosion.explode = false;
        rb = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();
        anim.speed = 0f;
    }

    private void Update()
    {
        if (transform.position.y <= 2f) tick = true;

        if (tick)
        {
            time += Time.deltaTime;
            anim.speed = time * 2f;

            if (time >= explodeTime && !explosion.explode)
            {
                explosion.explode = true;
            }
        }
    }

    public void Despawn()
    {
        tick = false;
        time = 0f;
        anim.speed = 0f;
        rb.velocity = Vector3.zero;
        spawner.Despawn();
        gameObject.SetActive(false);
    }
}
