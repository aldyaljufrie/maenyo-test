using System.Linq;
using UnityEngine;

public class Ragdolls : MonoBehaviour
{
    public static Ragdolls Instance;
    public PlayerState state = PlayerState.ALIVE;
    public CharacterController controller;
    public bool onRagdoll;
    public bool onAttack;
    public int hitTime = 0;
    public Transform SpawnPoint;
    private Rigidbody[] ragdolls;
    private Rigidbody playerRb;
    private Animator anim;
    private Vector3 impact = Vector3.zero;
    private float wakeupTime;
    private ThirdPersonMovement tpm;
    Camera cam;
    public enum PlayerState
    {
        ALIVE,
        DEAD,
        DIZZY
    }

    private void Awake()
    {
        Instance = this;
        anim = GetComponent<Animator>();
        playerRb = GetComponent<Rigidbody>();
        tpm = GetComponent<ThirdPersonMovement>();
        ragdolls = GetComponentsInChildren<Rigidbody>();
        cam = Camera.main;
        wakeupTime = 1f;
    }

    private void Update()
    {
        HandleState();
    }

    private void FixedUpdate()
    {
        if (hitTime >= 25f)
        {
            state = PlayerState.DIZZY;
            hitTime = 0;
        }

        if (impact.magnitude > 0.2) controller.Move(impact * Time.deltaTime);
        impact = Vector3.Lerp(impact, Vector3.zero, 5 * Time.deltaTime);
        anim.SetFloat("Hit", impact.magnitude);
    }

    private void HandleState()
    {
        switch (state)
        {
            case PlayerState.ALIVE:
                onRagdoll = false;
                AliveBehavior();
                break;
            case PlayerState.DIZZY:
                onRagdoll = true;
                RagdollBehavior();
                break;
            case PlayerState.DEAD:
                onRagdoll = true;
                RagdollBehavior();
                break;
        }


    }

    private void AliveBehavior()
    {
        anim.enabled = true;
        controller.enabled = true;
        foreach (var rb in ragdolls) rb.isKinematic = true;
        // playerRb.isKinematic = false;
    }

    private void RagdollBehavior()
    {
        anim.enabled = false;
        controller.enabled = false;

        foreach (var rb in ragdolls) rb.isKinematic = false;

        if (state == PlayerState.DIZZY && tpm.isGrounded && playerRb.velocity.magnitude < 1f)
        {
            wakeupTime -= Time.deltaTime;
            if (wakeupTime <= 0)
            {
                foreach (var rb in ragdolls) rb.isKinematic = true;
                state = PlayerState.ALIVE;
                wakeupTime = 1f;
            }
        }

        if (state == PlayerState.DEAD)
        {
            cam.GetComponent<OverheadCam>().target = null;
        }
    }

    public void IsAttacking(int value)
    {
        onAttack = value > 0;
    }

    public void TriggerRagdoll(Vector3 force, Vector3 hitpoint)
    {
        state = PlayerState.DIZZY;
        Rigidbody hitBody = ragdolls.OrderBy(body => Vector3.Distance(body.position, hitpoint)).First();
        hitBody.AddForceAtPosition(force, hitpoint, ForceMode.Impulse);
    }

    public void GetExplosion(Vector3 pos, float force, float radius, float lift)
    {
        state = PlayerState.DIZZY;
        playerRb.AddExplosionForce(force, pos, radius, lift);
    }
}
