using UnityEngine;

public class Obstacles : MonoBehaviour
{
    public Spawner spawner;
    public bool permadeath = false;
    public float speed = 10f;
    public float distance = 70f;
    public float forceMultiplier = 10f;
    public bool added = false;
    private Vector3 dir;
    private float spawnTime;
    [SerializeField] private bool isObstacles = true;

    private void Start()
    {
        dir = Vector3.forward;
    }

    private void Update()
    {
        if (spawner)
        {
            spawnTime += Time.deltaTime;
            transform.Translate(dir * speed * Time.deltaTime);
            distance = Vector3.Distance(transform.position, spawner.transform.position);

            if (distance >= spawner.spawnRadius && spawnTime >= 5f)
            {
                spawner.Despawn();
                speed = 10f;
                gameObject.SetActive(false);
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            if (isObstacles)
            {
                if (permadeath)
                {
                    Ragdolls.Instance.state = Ragdolls.PlayerState.DEAD;
                }
                else
                {
                    if (Ragdolls.Instance.state == Ragdolls.PlayerState.ALIVE)
                    {
                        added = true;
                        Ragdolls.Instance.TriggerRagdoll(other.transform.position * forceMultiplier, other.transform.position);
                        GameManager.instance.handlePoint(false, 10, other.transform.position);
                    }
                }
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player" && other.transform.name == "Player")
        {
            if (!isObstacles && !added && Ragdolls.Instance.state == Ragdolls.PlayerState.ALIVE)
            {
                added = true;
                GameManager.instance.handlePoint(true, 10, other.transform.position);
            }
        }
    }
}
