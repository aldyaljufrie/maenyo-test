using UnityEngine;

public class FloatingText : MonoBehaviour
{
    public Transform cam;
    void Start()
    {
        Destroy(gameObject, 1f);
        cam = Camera.main.transform;
    }

    private void Update()
    {
        if (cam)
        {
            transform.rotation = Quaternion.LookRotation(transform.position - cam.position);
        }
    }
}
