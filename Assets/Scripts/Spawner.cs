using UnityEngine;
using System.Collections.Generic;

public class Spawner : MonoBehaviour
{
    public static Spawner Instance;
    [SerializeField] GameObject prefab;
    public int spawnLimit = 2;
    public float speedMultiplier = 1f;
    public float maxSpawnLimit = 10f;
    public float spawnDelay = 2f;
    public float diffDelay = 30f;
    public float spawnRadius;
    bool spawn = true;
    public float spawnTimer, countTimer;
    int spawnCount;

    private void Awake()
    {
        Instance = this;
    }

    void FixedUpdate()
    {
        if (!GameManager.instance.onPause)
        {
            countTimer += Time.deltaTime;
            if (countTimer >= diffDelay)
            {
                countTimer = 0f;
                if (spawnLimit < maxSpawnLimit) spawnLimit++;
            }

            if (spawn)
            {
                spawnTimer += Time.deltaTime;
                if (spawnTimer > spawnDelay)
                {
                    if (spawnCount < spawnLimit)
                    {
                        Spawn();
                        spawnTimer = 0f;
                    }
                    else
                    {
                        spawn = false;
                    }
                }
            }
        }
    }

    public void Pause()
    {
        spawnTimer = 0f;
        spawnLimit = 2;
        countTimer = 0f;
        speedMultiplier = 1f;
        spawn = false;
        spawnCount = 0;
    }

    public void Play()
    {
        spawn = true;

        List<GameObject> pooled = GetComponent<ObjectPool>().pooledObjects;
        foreach (var obj in pooled)
        {
            Obstacles rope = obj.GetComponent<Obstacles>();
            rope.added = false;
            obj.SetActive(false);
        }
    }

    void Spawn()
    {
        GameObject spawned = GetComponent<ObjectPool>().GetPooledObject();
        if (spawned)
        {
            spawned.transform.position = GetSpawnPosition();
            spawned.transform.LookAt(transform.position);
            Obstacles rope = spawned.GetComponent<Obstacles>();
            rope.speed *= speedMultiplier;
            rope.spawner = this;
            rope.added = false;
            spawned.SetActive(true);
            spawnCount++;
        }
    }

    Vector3 GetSpawnPosition()
    {
        Vector3 pos = Random.insideUnitCircle.normalized * spawnRadius;
        return new Vector3(pos.x, transform.position.y, pos.y);
    }

    public void Despawn()
    {
        spawnCount--;
        spawn = true;
    }
}
